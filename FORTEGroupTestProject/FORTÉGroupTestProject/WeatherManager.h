//
//  WeatherManager.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 05.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherManager : NSObject

+ (void)safeCitiesToPlist:(NSArray *)array foFilename:(NSString *)filename;
+ (NSArray *)getCitiesArray:(NSString *)filename;
+ (NSString *)dataFilePath:(NSString *)filename;

@end
