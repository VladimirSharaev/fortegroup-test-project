//
//  CityTableViewCell.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryNameLabel;

@end
