//
//  Weather.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 04.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "Weather.h"

@implementation Weather

- (id)initWeatherWithDate:(NSDate *)date
{
    self = [super init];
    if (self) {
        self.date = date;
        
        self.morning = nil;
        self.afternoon = nil;
        self.evening = nil;
        self.night = nil;
        
        self.shortDescription = nil;
        self.longDescription = nil;
        self.icon = nil;
        
        self.windDegrees = nil;
        self.windSpeed = nil;
        
        self.humidity = nil;
        self.pressure = nil;
        
        self.clouds = nil;
    }
    return self;
}

#pragma  mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.date forKey:@"date"];
    
    [aCoder encodeObject:self.morning forKey:@"morning"];
    [aCoder encodeObject:self.afternoon forKey:@"afternoon"];
    [aCoder encodeObject:self.evening forKey:@"evening"];
    [aCoder encodeObject:self.night forKey:@"night"];
    
    [aCoder encodeObject:self.shortDescription forKey:@"shortDescription"];
    [aCoder encodeObject:self.longDescription forKey:@"longDescription"];
    [aCoder encodeObject:self.icon forKey:@"icon"];
    
    [aCoder encodeObject:self.windDegrees forKey:@"windDegrees"];
    [aCoder encodeObject:self.windSpeed forKey:@"windSpeed"];
    
    [aCoder encodeObject:self.humidity forKey:@"humidity"];
    [aCoder encodeObject:self.pressure forKey:@"pressure"];
    
    [aCoder encodeObject:self.clouds forKey:@"clouds"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.date = [aDecoder decodeObjectForKey:@"date"];
    
    self.morning = [aDecoder decodeObjectForKey:@"morning"];
    self.afternoon = [aDecoder decodeObjectForKey:@"afternoon"];
    self.evening = [aDecoder decodeObjectForKey:@"evening"];
    self.night = [aDecoder decodeObjectForKey:@"night"];
    
    self.shortDescription = [aDecoder decodeObjectForKey:@"shortDescription"];
    self.longDescription = [aDecoder decodeObjectForKey:@"longDescription"];
    self.icon = [aDecoder decodeObjectForKey:@"icon"];
    
    self.windDegrees = [aDecoder decodeObjectForKey:@"windDegrees"];
    self.windSpeed = [aDecoder decodeObjectForKey:@"windSpeed"];
    
    self.humidity = [aDecoder decodeObjectForKey:@"humidity"];
    self.pressure = [aDecoder decodeObjectForKey:@"pressure"];
    
    self.clouds = [aDecoder decodeObjectForKey:@"clouds"];
    
    return  self;
}

@end
