//
//  City.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "City.h"

@implementation City

- (id)initCityWithID:(NSNumber *)cityID cityName:(NSString *)cityName countryCode:(NSString *)countryCode coordinates:(NSDictionary *)coordinates
{
    self = [super init];
    if (self) {
        self.cityId = cityID;
        self.name = cityName;
        self.countryCode = countryCode;
        self.coordinates = coordinates;
    }
    return self;
}

#pragma  mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.cityId forKey:@"cityId"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.countryCode forKey:@"countryCode"];
    [aCoder encodeObject:self.coordinates forKey:@"coordinates"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.cityId = [aDecoder decodeObjectForKey:@"cityId"];
    self.name = [aDecoder decodeObjectForKey:@"name"];
    self.countryCode = [aDecoder decodeObjectForKey:@"countryCode"];
    self.coordinates = [aDecoder decodeObjectForKey:@"coordinates"];
    
    return  self;
}

@end
