//
//  Weather.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 04.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weather : NSObject

@property (nonatomic) NSDate *date;

@property (nonatomic) NSNumber *morning;
@property (nonatomic) NSNumber *afternoon;
@property (nonatomic) NSNumber *evening;
@property (nonatomic) NSNumber *night;

@property (nonatomic) NSString *shortDescription;
@property (nonatomic) NSString *longDescription;
@property (nonatomic) NSString *icon;

@property (nonatomic) NSNumber *windDegrees;
@property (nonatomic) NSNumber *windSpeed;

@property (nonatomic) NSNumber *humidity;
@property (nonatomic) NSNumber *pressure;

@property (nonatomic) NSNumber *clouds;

#pragma mark - Constructors
- (id)initWeatherWithDate:(NSDate *)date;

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;

@end
