//
//  MapViewController.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 05.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>
#import "CitiesManager.h"
#import "DaysOfWeatherViewController.h"
#import "AppDelegate.h"

@interface MapViewController () <MKMapViewDelegate>

@property (nonatomic) MKPointAnnotation *annotation;
@property (nonatomic) City *findedCity;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]
                                                                initWithTarget:self action:@selector(handleLongPress:)];
    longPressGestureRecognizer.minimumPressDuration = 2.0;
    [self.mapView addGestureRecognizer:longPressGestureRecognizer];
    
    self.annotation = [[MKPointAnnotation alloc] init];
    
    [self showAllertViewWithMessage:@"Select a city by long press"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Metgods

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D touchMapCoordinate = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    
    [self findCityNearTheTap:touchMapCoordinate];
}

- (void)findCityNearTheTap:(CLLocationCoordinate2D)touchMapCoordinate
{
    double latitude = touchMapCoordinate.latitude;
    double longitude = touchMapCoordinate.longitude;
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        NSArray *allCities = delegate.citiesArray;
        
        City *findedCity = nil;
        double minRange = MAXFLOAT;
        
        for (City *city in allCities) {
            double lonCity = [city.coordinates[@"lon"] doubleValue];
            double latCity = [city.coordinates[@"lat"] doubleValue];
            
            lonCity = lonCity - longitude;
            latCity = latCity - latitude;
            
            double range = sqrt(lonCity * lonCity + latCity * latCity);
            if (range < minRange) {
                minRange = range;
                findedCity = city;
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            self.findedCity = findedCity;
            
            CLLocationCoordinate2D coordinate;
            coordinate.longitude = (CLLocationDegrees)[findedCity.coordinates[@"lon"] doubleValue];
            coordinate.latitude = (CLLocationDegrees)[findedCity.coordinates[@"lat"] doubleValue];
            
            [self.annotation setCoordinate:coordinate];
            [self.annotation setTitle:findedCity.name];
            [self.mapView addAnnotation:self.annotation];
        });
    });
}

- (void)showAllertViewWithMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Help"
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alertController addAction:ok];
}

#pragma mark MKMapViewDelegate

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    DaysOfWeatherViewController *daysOfWeatherViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DaysOfWeatherViewController"];
    daysOfWeatherViewController.cityID = [self.findedCity.cityId stringValue];
    daysOfWeatherViewController.cityName = self.findedCity.name;
    [self.navigationController pushViewController:daysOfWeatherViewController animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
            pinView.enabled = YES;
            pinView.canShowCallout = YES;
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            
            pinView.rightCalloutAccessoryView = btn;
        } else {
            pinView.annotation = annotation;
        }
        return pinView;
    }
    return nil;
}

@end
