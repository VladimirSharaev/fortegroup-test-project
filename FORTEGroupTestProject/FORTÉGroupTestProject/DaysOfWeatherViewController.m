//
//  DaysOfWeatherViewController.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "DaysOfWeatherViewController.h"
#import "WeatherTableViewCell.h"
#import <OpenWeatherMapAPI/OWMWeatherAPI.h>
#import "ImageManager.h"
#import "Weather.h"
#import "DetailWeatherViewController.h"
#import "WeatherManager.h"

@interface DaysOfWeatherViewController ()

@property (nonatomic) OWMWeatherAPI *weatherAPI;
@property (nonatomic) NSArray *weatherArray;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *dataLabel;

@end

@implementation DaysOfWeatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //tableView settings
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //OpenWeather
    self.weatherAPI = [[OWMWeatherAPI alloc] initWithAPIKey:@"eba47effea88b18d5b67eae531209447"];
    [self.weatherAPI setTemperatureFormat:kOWMTempCelcius];
    
    self.title = self.cityName;
    
    [self getWeatherByCityID];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void)getWeatherByCityID
{
    [self.weatherAPI forecastWeatherByCityId:self.cityID withCallback:^(NSError *error, NSDictionary *result){
        if (error) {
            NSArray *array = [WeatherManager getCitiesArray:self.cityID];
            if (array) {
                self.weatherArray = array;
                [self.tableView reloadData];
                [self.activityIndicator stopAnimating];
                
                [self showAllertViewWithMessage:@"No internet connection, but we loaded the last data."];
            }else{
                [self showAllertViewWithMessage:@"No internet connection"];
                
                self.tableView.hidden = YES;
                self.dataLabel.hidden = NO;
                [self.activityIndicator stopAnimating];
            }
            
            return;
        }
        if ([result[@"cod"] isEqual:@"404"]) {
            [self showAllertViewWithMessage:@"Not found city"];
            
            return;
        }
        
        self.weatherArray = [self parseResultArray:result[@"list"]];
        [WeatherManager safeCitiesToPlist:self.weatherArray foFilename:self.cityID];
        [self.tableView reloadData];
        [self.activityIndicator stopAnimating];
    }];
}

- (NSArray *)parseResultArray:(NSArray *)array
{
    NSMutableArray *sortedMutableArray = [NSMutableArray new];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    for (NSDictionary *dictionary in array) {
        NSDate *date = dictionary[@"dt"];
        
        NSDateComponents *firstDateComponents = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay |NSCalendarUnitHour) fromDate:date];
        
        BOOL isThatDayInSortedArray = NO;
        Weather *weatherToChange = nil;
        
        for (Weather *weather in sortedMutableArray) {
            
            NSDateComponents *secondDateComponents = [gregorianCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay |NSCalendarUnitHour) fromDate:weather.date];
            
            if ([firstDateComponents day] - [secondDateComponents day] < 1) {
                weatherToChange = weather;
                
                isThatDayInSortedArray = YES;
            }else{
                isThatDayInSortedArray = NO;
            }
        }
        
        if (!isThatDayInSortedArray) {
            Weather *weather = [[Weather alloc] initWeatherWithDate:date];
            weather = [self addTemperetureToWeather:weather forHour:[firstDateComponents hour]  fromDictionaty:dictionary];
            
            weather.shortDescription = [dictionary[@"weather"] firstObject][@"main"];
            weather.longDescription = [dictionary[@"weather"] firstObject][@"description"];
            weather.icon = [dictionary[@"weather"] firstObject][@"icon"];
            
            weather.windDegrees = dictionary[@"wind"][@"deg"];
            weather.windSpeed = dictionary[@"wind"][@"speed"];
            
            weather.humidity = dictionary[@"main"][@"humidity"];
            weather.pressure = dictionary[@"main"][@"pressure"];
            
            weather.clouds = dictionary[@"clouds"][@"all"];
            
            [sortedMutableArray addObject:weather];
        }else{
            weatherToChange = [self addTemperetureToWeather:weatherToChange forHour:[firstDateComponents hour] fromDictionaty:dictionary];
        }
    }
    
    
    NSArray *sortedArray = [NSArray arrayWithArray:sortedMutableArray];
    return [self getRidOfTheEmptyValues:sortedArray];
}

- (Weather *)addTemperetureToWeather:(Weather *)weather forHour:(NSInteger)hour fromDictionaty:(NSDictionary *)dictionary
{
    if (hour == 0 || hour == 3) {
        weather.night = dictionary[@"main"][@"temp"];
    }
    if (hour == 6 || hour == 9) {
        weather.morning = dictionary[@"main"][@"temp"];
    }
    if (hour == 12 || hour == 15) {
        weather.afternoon = dictionary[@"main"][@"temp"];
    }
    if (hour == 18 || hour == 21) {
        weather.evening = dictionary[@"main"][@"temp"];
    }
    
    return weather;
}

- (NSArray *)getRidOfTheEmptyValues:(NSArray *)array
{
    for (Weather *weather in array) {
        for (NSInteger i = 0; i < 4; i++) {
            if (weather.night == nil) {
                weather.night = weather.evening;
            }
            if (weather.morning == nil) {
                weather.morning = weather.night;
            }
            if (weather.afternoon == nil) {
                weather.afternoon = weather.morning;
            }
            if (weather.evening == nil) {
                weather.evening = weather.afternoon;
            }
        }
    }
    
    return array;
}

- (void)showAllertViewWithMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Error"
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alertController addAction:ok];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.weatherArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"WeatherTableViewCell";
    WeatherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[WeatherTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.tag = indexPath.row;
    
    
    Weather *weather = [self.weatherArray objectAtIndex:indexPath.row];
    
    [cell.shortDescriptionOfWeatherLabel setText:weather.shortDescription];
    [cell.longDescriptionOfWeatherLabel setText:weather.longDescription];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:usLocale];
    [dateFormatter setDateFormat:@"d MMM"];
    [cell.dataLabel setText:[dateFormatter stringFromDate:weather.date]];
    
    NSString *urlImage = [NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png", weather.icon];
    [cell.descriptionImageView setImage:nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       UIImage *image = [ImageManager getImage:weather.icon];
                       if (!image) {
                           NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlImage]];
                           if (imageData) {
                               dispatch_sync(dispatch_get_main_queue(), ^{
                                   NSInteger index = indexPath.section * 2 + indexPath.row;
                                   if (cell.tag == index) {
                                       [cell.descriptionImageView setImage:[UIImage imageWithData:imageData]];
                                   }
                               });
                               [ImageManager saveImage:[UIImage imageWithData:imageData] filename:weather.icon];
                           }
                       }else{
                           dispatch_sync(dispatch_get_main_queue(), ^{
                               NSInteger index = indexPath.section * 2 + indexPath.row;
                               if (cell.tag == index) {
                                   [cell.descriptionImageView setImage:image];
                               }
                           });
                       }
                   });
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSelector:@selector(deselectRowAtIndexPath:) withObject:indexPath afterDelay:0.3];
    
    Weather *weather = [self.weatherArray objectAtIndex:indexPath.row];
    
    DetailWeatherViewController *detailWeatherViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailWeatherViewController"];
    detailWeatherViewController.weather = weather;
    [self.navigationController pushViewController:detailWeatherViewController animated:YES];
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
