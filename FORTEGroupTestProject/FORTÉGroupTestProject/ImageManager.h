//
//  ImageManager.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 03.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h> 

@interface ImageManager : NSObject

+ (void)saveImage:(UIImage *)image filename:(NSString *)filename;
+ (UIImage *)getImage:(NSString *)filename;

@end
