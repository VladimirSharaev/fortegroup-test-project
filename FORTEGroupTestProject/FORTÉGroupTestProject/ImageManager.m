//
//  ImageManager.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 03.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "ImageManager.h"

@implementation ImageManager

+ (void)saveImage:(UIImage *)image filename:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/ImageFolder"];
    filename = [dataPath stringByAppendingFormat:@"/%@", filename];
    NSData * data = UIImagePNGRepresentation(image);
    [data writeToFile:filename atomically:YES];
}

+ (UIImage *)getImage:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/ImageFolder"];
    filename = [dataPath stringByAppendingFormat:@"/%@", filename];
    NSData * data = [NSData dataWithContentsOfFile:filename];
    return [UIImage imageWithData:data];
}

@end
