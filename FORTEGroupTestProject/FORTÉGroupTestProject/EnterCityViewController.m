//
//  EnterCityViewController.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "EnterCityViewController.h"
#import "CitiesViewController.h"
#import "MapViewController.h"

@interface EnterCityViewController ()

@property (weak, nonatomic) IBOutlet UITextField *cityNameTextField;

- (IBAction)searchButtonWasPressed:(UIButton *)sender;
- (IBAction)mapButtonWasPressed:(id)sender;
@end

@implementation EnterCityViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return true;
}

#pragma mark - UIResponder
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - Action

- (IBAction)searchButtonWasPressed:(UIButton *)sender
{
    if ([self.cityNameTextField hasText]) {
        
        CitiesViewController *citiesViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CitiesViewController"];
        citiesViewController.cityName = self.cityNameTextField.text;
        [self.navigationController pushViewController:citiesViewController animated:YES];
        
        
    }else{
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Error"
                                              message:@"Enter city name"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                             }];
        
        [alertController addAction:ok];
    }
}

- (IBAction)mapButtonWasPressed:(id)sender
{
    MapViewController *mapViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    [self.navigationController pushViewController:mapViewController animated:YES];
}
@end
