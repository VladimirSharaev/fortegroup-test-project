//
//  DetailWeatherViewController.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 04.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "DetailWeatherViewController.h"

@interface DetailWeatherViewController ()

@property (weak, nonatomic) IBOutlet UILabel *morningLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *eveningLabel;
@property (weak, nonatomic) IBOutlet UILabel *nightLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *cloudLabel;
@property (weak, nonatomic) IBOutlet UILabel *windSpeedLabel;

@property (weak, nonatomic) IBOutlet UIImageView *windDirectionImageView;
@end

@implementation DetailWeatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:usLocale];
    [dateFormatter setDateFormat:@"d MMM"];
    self.title = [dateFormatter stringFromDate:self.weather.date];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"0.#"];
    
    NSString *morning = [NSString stringWithFormat:@"%@°C",[numberFormatter stringFromNumber:self.weather.morning]];
    NSString *day = [NSString stringWithFormat:@"%@°C",[numberFormatter stringFromNumber:self.weather.afternoon]];
    NSString *evening = [NSString stringWithFormat:@"%@°C",[numberFormatter stringFromNumber:self.weather.evening]];
    NSString *night = [NSString stringWithFormat:@"%@°C",[numberFormatter stringFromNumber:self.weather.night]];
    
    NSString *pressure = [NSString stringWithFormat:@"%@ hpa",[numberFormatter stringFromNumber:self.weather.pressure]];
    NSString *humidity = [NSString stringWithFormat:@"%@%%",[numberFormatter stringFromNumber:self.weather.humidity]];
    NSString *clouds = [NSString stringWithFormat:@"%@%%",[numberFormatter stringFromNumber:self.weather.clouds]];
    NSString *windSpeed = [NSString stringWithFormat:@"%@m/s",[numberFormatter stringFromNumber:self.weather.windSpeed]];
    
    [self.morningLabel setText:morning];
    [self.dayLabel setText:day];
    [self.eveningLabel setText:evening];
    [self.nightLabel setText:night];
    
    [self.pressureLabel setText:pressure];
    [self.humidityLabel setText:humidity];
    [self.cloudLabel setText:clouds];
    [self.windSpeedLabel setText:windSpeed];
    
    double windDegrees = [self.weather.windDegrees doubleValue];
    self.windDirectionImageView.transform = CGAffineTransformMakeRotation(M_PI * 2 * windDegrees / 348.75f);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
