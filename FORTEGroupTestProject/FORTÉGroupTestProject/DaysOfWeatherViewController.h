//
//  DaysOfWeatherViewController.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DaysOfWeatherViewController : UIViewController

@property (nonatomic) NSString *cityID;
@property (nonatomic) NSString *cityName;

@end
