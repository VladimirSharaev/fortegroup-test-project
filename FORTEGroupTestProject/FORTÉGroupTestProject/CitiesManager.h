//
//  CitiesManager.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "City.h"

@interface CitiesManager : NSObject

+ (void)safeCitiesToPlist:(NSArray *)array;
+ (NSArray *)getCitiesArray;

@end
