//
//  CitiesManager.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "CitiesManager.h"

@implementation CitiesManager

+ (void)safeCitiesToPlist:(NSArray *)array
{
    BOOL a = [NSKeyedArchiver archiveRootObject:array toFile:[self dataFilePath]];
    NSLog(a ? @"Yes" : @"No");
}

+ (NSArray *)getCitiesArray
{
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:[self dataFilePath]];
    return array;
}

+ (NSString *)dataFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"Cities"];
}

@end
