//
//  CitiesViewController.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "CitiesViewController.h"
#import "CitiesManager.h"
#import "CityTableViewCell.h"
#import "DaysOfWeatherViewController.h"
#import "AppDelegate.h"

@interface CitiesViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSArray *citiesArray;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *cityNotfoundLabel;

@end

@implementation CitiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //tableView settings
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.citiesArray = [NSArray new];
    [self sortAllCitiesByEnteredName];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void)sortAllCitiesByEnteredName
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        NSArray *allCities = delegate.citiesArray;
        
        self.cityName = [self.cityName capitalizedString];
        
        NSMutableArray *newArray = [NSMutableArray new];
        for (City *city in allCities) {
            if ([city.name rangeOfString:self.cityName options:NSAnchoredSearch].location != NSNotFound)
            {
                [newArray addObject:city];
            }
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        NSArray *sortedArray = [NSArray arrayWithArray:newArray];
        sortedArray = [sortedArray sortedArrayUsingDescriptors:@[sortDescriptor]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            self.citiesArray = sortedArray;
            
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
            
            if (self.citiesArray.count == 0) {
                self.cityNotfoundLabel.hidden = NO;
                self.tableView.hidden = YES;
            }
        });
    });
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.citiesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CityTableViewCell";
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[CityTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    City *city = [self.citiesArray objectAtIndex:indexPath.row];
    
    [cell.cityNameLabel setText:city.name];
    [cell.countryNameLabel setText:city.countryCode];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSelector:@selector(deselectRowAtIndexPath:) withObject:indexPath afterDelay:0.3];
    
    City *city = [self.citiesArray objectAtIndex:indexPath.row];
    
    DaysOfWeatherViewController *daysOfWeatherViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DaysOfWeatherViewController"];
    daysOfWeatherViewController.cityID = [city.cityId stringValue];
    daysOfWeatherViewController.cityName = city.name;
    [self.navigationController pushViewController:daysOfWeatherViewController animated:YES];
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
