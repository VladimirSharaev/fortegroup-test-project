//
//  WeatherTableViewCell.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dataLabel;
@property (weak, nonatomic) IBOutlet UILabel *shortDescriptionOfWeatherLabel;
@property (weak, nonatomic) IBOutlet UILabel *longDescriptionOfWeatherLabel;
@property (weak, nonatomic) IBOutlet UIImageView *descriptionImageView;

@end
