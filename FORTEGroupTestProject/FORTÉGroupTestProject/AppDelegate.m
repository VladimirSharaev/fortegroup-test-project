//
//  AppDelegate.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "AppDelegate.h"
#import "CitiesManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //Load cities
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *userData = [userDefaults objectForKey:@"CountOfLoad"];
    if (!userData) {
        [self parseCitiesFromFile];

        NSNumber *countOfLoad = @(1);

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:countOfLoad];
        [userDefaults setObject:userData forKey:@"CountOfLoad"];
        [userDefaults synchronize];
    }else{
        self.citiesArray = [CitiesManager getCitiesArray];
    }
    
    //Folder of images
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/ImageFolder"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
        NSURL *localURL = [NSURL fileURLWithPath:dataPath];
        [self addSkipBackupAttributeToItemAtURL:localURL];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - Methods

- (void)parseCitiesFromFile
{
    NSString *filePathToCities = [[NSBundle mainBundle] pathForResource:@"city.list" ofType:@"json"];
    NSData *dataCities = [NSData dataWithContentsOfFile:filePathToCities];
    
    NSString *dataString = [[NSString alloc] initWithData:dataCities encoding:NSUTF8StringEncoding];
    
    dataString = [dataString stringByReplacingOccurrencesOfString:@"}}" withString:@"}},"];
    dataString = [dataString stringByReplacingOccurrencesOfString:@",]" withString:@"]"];
    
    NSData* newData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSArray *arrayOfCities = [NSJSONSerialization JSONObjectWithData:newData options:kNilOptions error:nil];
    
    NSMutableArray *mutableArrayOfCities = [NSMutableArray new];
    for (NSDictionary *dictionary in arrayOfCities) {
        NSString *cityID = dictionary[@"_id"];
        NSString *name = dictionary[@"name"];
        NSString *countryCode = dictionary[@"country"];
        NSDictionary *coordinates = dictionary[@"coord"];
        
        City *city = [[City alloc] initCityWithID:cityID cityName:name countryCode:countryCode coordinates:coordinates];
        
        [mutableArrayOfCities addObject:city];
    }
    
    [CitiesManager safeCitiesToPlist:mutableArrayOfCities];
    self.citiesArray = mutableArrayOfCities;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

@end
