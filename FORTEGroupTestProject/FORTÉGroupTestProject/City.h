//
//  City.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property (nonatomic) NSNumber *cityId;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *countryCode;
@property (nonatomic) NSDictionary *coordinates;


#pragma mark - Constructors
- (id)initCityWithID:(NSString *)cityID cityName:(NSString *)cityName countryCode:(NSString *)countryCode coordinates:(NSDictionary *)coordinates;

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;
@end
