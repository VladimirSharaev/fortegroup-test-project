//
//  CitiesViewController.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 02.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitiesViewController : UIViewController

@property (nonatomic) NSString *cityName;

@end
