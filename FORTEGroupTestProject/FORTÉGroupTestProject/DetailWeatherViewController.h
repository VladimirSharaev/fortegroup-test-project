//
//  DetailWeatherViewController.h
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 04.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Weather.h"

@interface DetailWeatherViewController : UIViewController

@property (nonatomic) Weather *weather;

@end
