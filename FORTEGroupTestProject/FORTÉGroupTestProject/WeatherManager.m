//
//  WeatherManager.m
//  FORTÉGroupTestProject
//
//  Created by Sharaev Vladimir on 05.12.15.
//  Copyright © 2015 Sharaev Vladimir. All rights reserved.
//

#import "WeatherManager.h"

@implementation WeatherManager

+ (void)safeCitiesToPlist:(NSArray *)array foFilename:(NSString *)filename
{
    [NSKeyedArchiver archiveRootObject:array toFile:[self dataFilePath:filename]];
}

+ (NSArray *)getCitiesArray:(NSString *)filename
{
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithFile:[self dataFilePath:filename]];
    return array;
}

+ (NSString *)dataFilePath:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:filename];
}

@end
